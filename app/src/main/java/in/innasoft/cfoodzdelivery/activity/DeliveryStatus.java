package in.innasoft.cfoodzdelivery.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;

import in.innasoft.cfoodzdelivery.Connectivity;
import in.innasoft.cfoodzdelivery.Globals;
import in.innasoft.cfoodzdelivery.R;
import in.innasoft.cfoodzdelivery.model.Orders;

public class DeliveryStatus extends AppCompatActivity implements View.OnClickListener {

    public static Orders orders;
    Button buttonBack, buttonSubmit;
    EditText editReason;
    RadioGroup radioGroup;
    LinearLayout linearLayout;
    String status;
    TextView textTitle, textPrice, orderStatus;
    boolean buttonClick;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_status);

        buttonClick = false;

        buttonBack = findViewById(R.id.buttonBack);
        buttonSubmit = findViewById(R.id.buttonSubmit);
        textTitle = findViewById(R.id.textTitle);
        textPrice = findViewById(R.id.textPrice);
        orderStatus = findViewById(R.id.orderStatus);
        editReason = findViewById(R.id.edit_reasons);
        radioGroup = findViewById(R.id.radioGroup);
        linearLayout = findViewById(R.id.linear);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        if (orders != null) {
            textTitle.setText(orders.name);
            textPrice.setText(Html.fromHtml("Total price : " + "<b>" + "\u20B9 " + orders.finalPrice + "</b>"));
            orderStatus.setText(Html.fromHtml("Paid via : " + "<b>" + orders.getawayName + "</b> "));
        }

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int selected) {
                if (selected == R.id.checkboxYes) {
                    status = "7";
                    linearLayout.setVisibility(View.GONE);
                } else if (selected == R.id.checkboxNo) {
                    status = "8";
                    linearLayout.setVisibility(View.VISIBLE);
                }
            }
        });

        buttonSubmit.setOnClickListener(this);
        buttonBack.setOnClickListener(this);

    }

    @Override
    public void onBackPressed() {
        if (buttonClick) {
            super.onBackPressed();
            finish();
        } else {
            showDialogOK("Please submit the delivery status", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            dialog.dismiss();
                            break;
                    }
                }
            });
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonBack:
                if (buttonClick) {
                    finish();
                } else {
                    showDialogOK("Please submit the delivery status", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case DialogInterface.BUTTON_POSITIVE:
                                    dialog.dismiss();
                                    break;
                            }
                        }
                    });
                }
                break;
            case R.id.buttonSubmit:
                if (Connectivity.isConnected(DeliveryStatus.this)) {
                    progressDialog.show();
                    postOrderStatus();
                } else {
                    Snackbar snackbar = Snackbar.make(DeliveryStatus.this.getWindow().getDecorView().getRootView(), "No Internet Connection...!", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
                break;
        }
    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this).setMessage(message).setPositiveButton("OK", okListener).create().show();
    }

    private void postOrderStatus() {

        HashMap<String, String> headers = new HashMap<>();
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", Globals.user.userId);
        params.put("order_id", orders.id);
        params.put("status", status);
        if (editReason.getText().length() > 0)
            params.put("remark", editReason.getText().toString());

        Globals.POST(Globals.getBaseURL() + "orders/status", headers, params, new Globals.VolleyCallback() {
            @Override
            public void onSuccess(String result) {
                buttonClick = true;
                progressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.getString("status").equalsIgnoreCase("10100")) {
                        Toast.makeText(DeliveryStatus.this, "Delivered Successfully", Toast.LENGTH_SHORT).show();
                        finish();
                    } else if (jsonObject.getString("status").equalsIgnoreCase("10200")) {
                        Toast.makeText(DeliveryStatus.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFail(String result) {
                progressDialog.dismiss();
                buttonClick = true;
                Toast.makeText(DeliveryStatus.this, result, Toast.LENGTH_SHORT).show();

            }
        });

    }

}
