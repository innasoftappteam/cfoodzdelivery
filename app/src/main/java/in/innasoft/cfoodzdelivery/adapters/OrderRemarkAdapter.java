package in.innasoft.cfoodzdelivery.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import in.innasoft.cfoodzdelivery.R;
import in.innasoft.cfoodzdelivery.holders.OrderViewHolder;
import in.innasoft.cfoodzdelivery.itemClickListerners.OrderViewItemClickListener;
import in.innasoft.cfoodzdelivery.model.OrderRemark;

public class OrderRemarkAdapter extends RecyclerView.Adapter<OrderViewHolder> {

    private ArrayList<OrderRemark> orderViewModels;
    public Context context;
    private LayoutInflater li;
    private int resource;
    private Typeface typeface;

    public OrderRemarkAdapter(ArrayList<OrderRemark> orderViewModels, Context context, int resource) {
        this.orderViewModels = orderViewModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.fonttype_one));
    }

    @NonNull
    @Override
    public OrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        return new OrderViewHolder(layout);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull OrderViewHolder holder, final int position) {

        holder.product_name_txt.setText("Order Status : "+orderViewModels.get(position).order_status);
        holder.product_name_txt.setTypeface(typeface);

        holder.quantity_txt.setText("Message : "+orderViewModels.get(position).message);
        holder.quantity_txt.setTypeface(typeface);

        holder.setItemClickListener(new OrderViewItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return this.orderViewModels.size();
    }
}

