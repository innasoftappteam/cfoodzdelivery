package in.innasoft.cfoodzdelivery.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import in.innasoft.cfoodzdelivery.Connectivity;
import in.innasoft.cfoodzdelivery.Globals;
import in.innasoft.cfoodzdelivery.R;
import in.innasoft.cfoodzdelivery.activity.MainActivity;
import in.innasoft.cfoodzdelivery.activity.NavigationActivity;
import in.innasoft.cfoodzdelivery.activity.OrderDetailsActivity;
import in.innasoft.cfoodzdelivery.model.Orders;
import in.innasoft.cfoodzdelivery.model.User;

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.MyViewHolder> {

    Context context;
    ArrayList<Orders> ordersArrayList;

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView orderId, location, totalPrice, amtStatus, customerName;
        ImageView productImage, imageInfo, imageNavigate, imageCall;
        Button acceptOrder;

        public MyViewHolder(View view) {
            super(view);
            orderId = view.findViewById(R.id.orderId);
            location = view.findViewById(R.id.location);
            totalPrice = view.findViewById(R.id.total_price);
            amtStatus = view.findViewById(R.id.amtStatus);
            customerName = view.findViewById(R.id.customerName);
            productImage = view.findViewById(R.id.productImage);
            imageInfo = view.findViewById(R.id.info);
            imageCall = view.findViewById(R.id.callIcon);
            imageNavigate = view.findViewById(R.id.navGoogleMaps);
            acceptOrder = view.findViewById(R.id.acceptOrder);
        }
    }

    public OrdersAdapter(Context context, ArrayList<Orders> orders) {
        this.context = context;
        this.ordersArrayList = orders;
    }

//    public OrdersAdapter(Context context, ArrayList<Orders> orders) {
//        this.context = context;
//        this.ordersArrayList = orders;
//    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_orders, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Orders order = ordersArrayList.get(position);

        if (order.name != null && !order.name.isEmpty()){
            holder.customerName.setText(Globals.firstLetterUppercase(order.name));
        } else {
            holder.customerName.setText("");
        }

        if (order.images != null && !order.images.isEmpty())
            Picasso.with(context).load(order.images).error(R.mipmap.ic_launcher_round).placeholder(R.mipmap.ic_launcher_round).into(holder.productImage);
        else
            holder.productImage.setImageResource(R.mipmap.ic_launcher_round);

        if (order.referenceId != null && order.referenceId.length() > 0) {
            holder.orderId.setText("Order Id : "+order.referenceId);
        } else {
            holder.orderId.setText("");
        }

        if (order.finalPrice != null && order.finalPrice.length() > 0){
            holder.totalPrice.setText("\u20B9 "+order.finalPrice);
        } else {
            holder.totalPrice.setText("");
        }

        if (order.area != null && order.area.length() > 0) {
            holder.location.setText(order.area);
        } else {
            holder.location.setText("");
        }

        if (order.getawayName != null && order.getawayName.length() > 0){
            holder.amtStatus.setText("Paid via : "+order.getawayName);
        } else {
            holder.amtStatus.setText("");
        }

        if (order.status.equalsIgnoreCase("1")){
            holder.acceptOrder.setVisibility(View.VISIBLE);
            holder.imageNavigate.setVisibility(View.GONE);
        } else if (order.status.equalsIgnoreCase("6")){
            holder.acceptOrder.setVisibility(View.GONE);
            holder.imageNavigate.setVisibility(View.VISIBLE);
        } else if (order.status.equalsIgnoreCase("7")){
            holder.acceptOrder.setVisibility(View.GONE);
            holder.imageNavigate.setVisibility(View.GONE);
        }

        holder.imageInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Connectivity.isConnected(context))
                    Toast.makeText(context, "Please Check Your Internet Connection.", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(context, OrderDetailsActivity.class);
                intent.putExtra("id", ordersArrayList.get(position).id);
                intent.putExtra("ReferenceId", ordersArrayList.get(position).referenceId);
                context.startActivity(intent);

            }
        });

        holder.imageCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    Intent intentCall = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ordersArrayList.get(position).mobile));
                    context.startActivity(intentCall);
            }
        });

        holder.acceptOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Connectivity.isConnected(context)) {
                    acceptOrder(ordersArrayList.get(position).id, position);
                }else {
                    Toast.makeText(context, "Please Check Your Internet Connection.", Toast.LENGTH_SHORT).show();
                }

            }
        });

        holder.imageNavigate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                NavigationActivity.orders = ordersArrayList.get(position);
                Intent intent = new Intent(context, NavigationActivity.class);
                intent.putExtra("address", ordersArrayList.get(position).address);
                context.startActivity(intent);
            }
        });

    }

    private void acceptOrder(String id, final int pos) {
        HashMap<String,String> headers = new HashMap<>();
        HashMap<String,String> params = new HashMap<>();
        params.put("user_id", Globals.user.userId);
        params.put("order_id", id);

        Globals.POST(Globals.getBaseURL() + "proceed_shippment", headers, params, new Globals.VolleyCallback() {
            @Override
            public void onSuccess(String result) {

                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.getString("status").equalsIgnoreCase("10100")){
                        ordersArrayList.get(pos).status = "6";
                        notifyDataSetChanged();
                    } else {
                        Toast.makeText(context, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }

            @Override
            public void onFail(String result) {
                Toast.makeText(context, result, Toast.LENGTH_SHORT).show();
            }
        });


    }

    @Override
    public int getItemCount() {
        return ordersArrayList.size();
    }
}
