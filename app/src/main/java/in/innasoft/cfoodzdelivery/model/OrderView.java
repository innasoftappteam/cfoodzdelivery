package in.innasoft.cfoodzdelivery.model;

import org.json.JSONObject;

public class OrderView {

    public String id, user_id, user_type, name, email, mobile, country, state, city, area, address, pincode, latitude, longitude,
            reference_id, order_id, total_mrp_price, final_price, cancel_charges, refund_price, order_status, getaway_name, order_date,
            order_time, create_date_time,update_date_time;


    public OrderView(JSONObject jsonObject) {

        try {
            if (jsonObject.has("id") && !jsonObject.isNull("id"))
                this.id = jsonObject.getString("id");
            if (jsonObject.has("user_id") && !jsonObject.isNull("user_id"))
                this.user_id = jsonObject.getString("user_id");
            if (jsonObject.has("user_type") && !jsonObject.isNull("user_type"))
                this.user_type = jsonObject.getString("user_type");
            if (jsonObject.has("name") && !jsonObject.isNull("name"))
                this.name = jsonObject.getString("name");
            if (jsonObject.has("email") && !jsonObject.isNull("email"))
                this.email = jsonObject.getString("email");
            if (jsonObject.has("mobile") && !jsonObject.isNull("mobile"))
                this.mobile = jsonObject.getString("mobile");
            if (jsonObject.has("country") && !jsonObject.isNull("country"))
                this.country = jsonObject.getString("country");
            if (jsonObject.has("state") && !jsonObject.isNull("state"))
                this.state = jsonObject.getString("state");
            if (jsonObject.has("city") && !jsonObject.isNull("city"))
                this.city = jsonObject.getString("city");
            if (jsonObject.has("area") && !jsonObject.isNull("area"))
                this.area = jsonObject.getString("area");
            if (jsonObject.has("address") && !jsonObject.isNull("address"))
                this.address = jsonObject.getString("address");
            if (jsonObject.has("pincode") && !jsonObject.isNull("pincode"))
                this.pincode = jsonObject.getString("pincode");
            if (jsonObject.has("latitude") && !jsonObject.isNull("latitude"))
                this.latitude = jsonObject.getString("latitude");
            if (jsonObject.has("longitude") && !jsonObject.isNull("longitude"))
                this.longitude = jsonObject.getString("longitude");
            if (jsonObject.has("reference_id") && !jsonObject.isNull("reference_id"))
                this.reference_id = jsonObject.getString("reference_id");
            if (jsonObject.has("order_id") && !jsonObject.isNull("order_id"))
                this.order_id = jsonObject.getString("order_id");
            if (jsonObject.has("total_mrp_price") && !jsonObject.isNull("total_mrp_price"))
                this.total_mrp_price = jsonObject.getString("total_mrp_price");
            if (jsonObject.has("final_price") && !jsonObject.isNull("final_price"))
                this.final_price = jsonObject.getString("final_price");
            if (jsonObject.has("cancel_charges") && !jsonObject.isNull("cancel_charges"))
                this.cancel_charges = jsonObject.getString("cancel_charges");
            if (jsonObject.has("refund_price") && !jsonObject.isNull("refund_price"))
                this.refund_price = jsonObject.getString("refund_price");
            if (jsonObject.has("order_status") && !jsonObject.isNull("order_status"))
                this.order_status = jsonObject.getString("order_status");
            if (jsonObject.has("getaway_name") && !jsonObject.isNull("getaway_name"))
                this.getaway_name = jsonObject.getString("getaway_name");
            if (jsonObject.has("order_date") && !jsonObject.isNull("order_date"))
                this.order_date = jsonObject.getString("order_date");
            if (jsonObject.has("order_time") && !jsonObject.isNull("order_time"))
                this.order_time = jsonObject.getString("order_time");
            if (jsonObject.has("create_date_time") && !jsonObject.isNull("create_date_time"))
                this.create_date_time = jsonObject.getString("create_date_time");
            if (jsonObject.has("update_date_time") && !jsonObject.isNull("update_date_time"))
                this.update_date_time = jsonObject.getString("update_date_time");

        }catch (Exception e){
            e.printStackTrace();
        }

    }
}