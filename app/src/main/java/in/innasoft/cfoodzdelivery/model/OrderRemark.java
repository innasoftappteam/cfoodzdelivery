package in.innasoft.cfoodzdelivery.model;

import org.json.JSONObject;

import in.innasoft.cfoodzdelivery.Globals;

public class OrderRemark {

    public String order_status, message, create_date_time;

    public OrderRemark(JSONObject jsonObject) {
        try {

            if (jsonObject.has("order_status") && !jsonObject.isNull("order_status"))
                this.order_status = jsonObject.getString("order_status");
            if (jsonObject.has("message") && !jsonObject.isNull("message"))
                this.message = jsonObject.getString("message");
            if (jsonObject.has("create_date_time") && !jsonObject.isNull("create_date_time"))
                this.create_date_time = jsonObject.getString("create_date_time");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
