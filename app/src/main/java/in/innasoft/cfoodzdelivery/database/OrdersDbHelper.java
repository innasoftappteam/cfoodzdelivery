package in.innasoft.cfoodzdelivery.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class OrdersDbHelper extends SQLiteOpenHelper
{
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "cfoodzdelivery.dp";

    private static final String TABLE_ORDERS = "orders";

    private static final String  ORDERID = "orderId";
    private static final String  NAME = "name";
    private static final String LOCATION = "location";
    private static final String PRICE = "price";
    private static final String GATEWAYNAME = "gatewayName";
    private static final String STATUS = "status";
    private static final String MOBILENUMBER = "mobilenumber";

    private static final String TABLE_ORDERDETAILS = "orderDetails";

    private static final String PRODUCTNAME = "productName";
    private static final String QUANTITY = "quantity";
    private static final String TOTALPRICE = "totalPrice";
    private static final String ADDRESS = "address";


    public OrdersDbHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public OrdersDbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_ORDERS_TABLE = "CREATE TABLE " + TABLE_ORDERS  + "("

                + ORDERID  + " VARCHAR ,"
                + NAME + " TEXT , "
                + LOCATION + " TEXT , "
                + PRICE + " TEXT , "
                + GATEWAYNAME + " TEXT , "
                + STATUS + " TEXT , "
                + MOBILENUMBER + " TEXT )";

        db.execSQL(CREATE_ORDERS_TABLE);

        String CREATE_TABLE_ORDERDETAILS = "CREATE TABLE " + TABLE_ORDERDETAILS  + "("

                + PRODUCTNAME  + " TEXT ,"
                + QUANTITY + " TEXT , "
                + PRICE + " TEXT , "
                + TOTALPRICE + " TEXT , "
                + ADDRESS + " TEXT )";

        db.execSQL(CREATE_TABLE_ORDERDETAILS);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ORDERS);
        onCreate(db);

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ORDERDETAILS);
        onCreate(db);
    }

    public void addOrderList(ContentValues contentValues){
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_ORDERS, null, contentValues);
        db.close();
    }

    public List<String> getOrderId() {
        List<String> orderId = new ArrayList<String>();

        String selectQuery = "SELECT  * FROM " + TABLE_ORDERS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                orderId.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        Log.d("DBGALLERYNAMES", orderId.toString());
        return orderId;
    }

    public List<String> getName() {
        List<String> orderNames = new ArrayList<String>();

        String selectQuery = "SELECT  * FROM " + TABLE_ORDERS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                orderNames.add(cursor.getString(1));
            } while (cursor.moveToNext());
        }
        Log.d("DBGALLERYNAMES", orderNames.toString());
        return orderNames;
    }

    public List<String> getLocation() {
        List<String> orderLocations = new ArrayList<String>();

        String selectQuery = "SELECT  * FROM " + TABLE_ORDERS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                orderLocations.add(cursor.getString(2));
            } while (cursor.moveToNext());
        }
        Log.d("DBGALLERYNAMES", orderLocations.toString());
        return orderLocations;
    }
    public List<String> getPrice() {
        List<String> orderPrice = new ArrayList<String>();

        String selectQuery = "SELECT  * FROM " + TABLE_ORDERS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                orderPrice.add(cursor.getString(3));
            } while (cursor.moveToNext());
        }
        Log.d("DBGALLERYIDS", orderPrice.toString());
        return orderPrice;
    }

    public List<String> getGateway() {
        List<String> orderGateway = new ArrayList<String>();

        String selectQuery = "SELECT  * FROM " + TABLE_ORDERS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                orderGateway.add(cursor.getString(4));
            } while (cursor.moveToNext());
        }

        return orderGateway;
    }

    public List<String> getStatus() {
        List<String> orderStatus = new ArrayList<String>();

        String selectQuery = "SELECT  * FROM " + TABLE_ORDERS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                orderStatus.add(cursor.getString(5));
            } while (cursor.moveToNext());
        }
        Log.d("DBGALLERYIDS", orderStatus.toString());
        return orderStatus;
    }

    public List<String> getMobilenumber() {
        List<String> orderMobilenumber = new ArrayList<String>();

        String selectQuery = "SELECT  * FROM " + TABLE_ORDERS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                orderMobilenumber.add(cursor.getString(6));
            } while (cursor.moveToNext());
        }
        Log.d("DBGALLERYIDS", orderMobilenumber.toString());
        return orderMobilenumber;
    }


    public void deleteOrderList()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TABLE_ORDERS);
        db.close();
    }



    public void addOrderViewList(ContentValues contentValues){
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_ORDERDETAILS, null, contentValues);
        db.close();
    }

    public List<String> getProductName() {
        List<String> productName = new ArrayList<String>();

        String selectQuery = "SELECT  * FROM " + TABLE_ORDERDETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                productName.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        return productName;
    }

    public List<String> getQuantity() {
        List<String> quantity = new ArrayList<String>();

        String selectQuery = "SELECT  * FROM " + TABLE_ORDERDETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                quantity.add(cursor.getString(1));
            } while (cursor.moveToNext());
        }

        return quantity;
    }

    public List<String> getPriceDetail() {
        List<String> priceDetail = new ArrayList<String>();

        String selectQuery = "SELECT  * FROM " + TABLE_ORDERDETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                priceDetail.add(cursor.getString(2));
            } while (cursor.moveToNext());
        }
        return priceDetail;
    }
    public List<String> getTotalPrice() {
        List<String> totalPrice = new ArrayList<String>();

        String selectQuery = "SELECT  * FROM " + TABLE_ORDERDETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                totalPrice.add(cursor.getString(3));
            } while (cursor.moveToNext());
        }

        return totalPrice;
    }

    public List<String> getAddress() {
        List<String> address = new ArrayList<String>();

        String selectQuery = "SELECT  * FROM " + TABLE_ORDERDETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                address.add(cursor.getString(4));
            } while (cursor.moveToNext());
        }

        return address;
    }

    public void deleteOrderViewList()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TABLE_ORDERDETAILS);
        db.close();
    }
}
